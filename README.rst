.. You should enable this project on travis-ci.org and coveralls.io to make
   these badges work. The necessary Travis and Coverage config files have been
   generated for you.

.. image:: https://travis-ci.org/memaldi/ckanext-welive_theme.svg?branch=master
    :target: https://travis-ci.org/memaldi/ckanext-welive_theme

.. image:: https://coveralls.io/repos/memaldi/ckanext-welive_theme/badge.png?branch=master
  :target: https://coveralls.io/r/memaldi/ckanext-welive_theme?branch=master

=============
ckanext-welive_theme
=============

The theme for WeLive's Open Data Stack.


------------
Requirements
------------

* CKAN 2.4.1

------------------------
Development Installation
------------------------

To install ckanext-welive_theme for development, activate your CKAN virtualenv and
do::

    git clone https://github.com/memaldi/ckanext-welive_theme.git
    cd ckanext-welive_theme
    python setup.py develop
    pip install -r dev-requirements.txt
