from ckan.controllers.package import PackageController
import ckan.lib.base as base
from pylons.controllers.util import redirect
from ckan.plugins import toolkit
from ckan.lib import helpers as h
import time
import ConfigParser
import os
import requests
import json

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

PLUGIN_SECTION = 'plugin:logging'
LOGGING_URL = config.get(PLUGIN_SECTION, 'logging_url')
APP_ID = config.get(PLUGIN_SECTION, 'app_id')


class ReportController(PackageController):
    def welive_report(self):
        lang = h.lang()
        if lang == 'sr_Latn':
            lang = 'sr_RS_latin'

        return redirect('/{}/contactus'.format(lang))


class WeLiveThemeController(base.BaseController):
    def welive_redirect(self, id):
        current_time = time.time()
        logged = False
        lang = h.lang()
        if lang == 'sr_Latn':
            lang = 'sr_RS_latin'
        if toolkit.c.userobj is not None:
            logged = True
        log_data = {'msg': 'Component selected',
                    'type': 'ComponentSelected',
                    'appID': APP_ID,
                    'timestamp': current_time,
                    }
        if id == 'home':
            log_data['custom_attr'] = {'componentname': 'Controller'}
            requests.post("{}/{}".format(LOGGING_URL, APP_ID),
                          data=json.dumps(log_data),
                          headers={'Content-type': 'application/json'})
            query = '/{}/overlay'.format(lang)
            if logged:
                query += '?login=true'
            return redirect(query)
        elif id == 'cdv':
            log_data['custom_attr'] = {'componentname': 'Citizen Data Vault'}
            requests.post("{}/{}".format(LOGGING_URL, APP_ID),
                          data=json.dumps(log_data),
                          headers={'Content-type': 'application/json'})
            query = '/cdv/{}/'.format(lang)
            if logged:
                query += 'user'
            return redirect(query)
        elif id == 'oia':
            log_data['custom_attr'] = {'componentname': 'Open Innovation Area'}
            requests.post("{}/{}".format(LOGGING_URL, APP_ID),
                          data=json.dumps(log_data),
                          headers={'Content-type': 'application/json'})
            query = '/{}/innovation-area/'.format(lang)
            if logged:
                query += '?login=true'
            return redirect(query)
        elif id == 'vc':
            log_data['custom_attr'] = {'componentname': 'Visual Composer'}
            requests.post("{}/{}".format(LOGGING_URL, APP_ID),
                          data=json.dumps(log_data),
                          headers={'Content-type': 'application/json'})
            query = '/visualcomposer/'
            if logged:
                query += '?login=true'
            return redirect(query)
        elif id == 'mkt':
            log_data['custom_attr'] = {'componentname': 'Marketplace'}
            requests.post("{}/{}".format(LOGGING_URL, APP_ID),
                          data=json.dumps(log_data),
                          headers={'Content-type': 'application/json'})
            query = '/{}/marketplace/'.format(lang)
            if logged:
                query += '?login=true'
            return redirect(query)
        elif id == 'ods':
            log_data['custom_attr'] = {'componentname': 'Open Data Stack'}
            requests.post("{}/{}".format(LOGGING_URL, APP_ID),
                          data=json.dumps(log_data),
                          headers={'Content-type': 'application/json'})
            query = '/ods/{}'.format(lang)
            if logged:
                query += '?login=true'
            return redirect(query)
        elif id == 'ads':
            log_data['custom_attr'] = {'componentname': 'Analytics Dashboard'}
            requests.post("{}/{}".format(LOGGING_URL, APP_ID),
                          data=json.dumps(log_data),
                          headers={'Content-type': 'application/json'})
            query = '/ads'
            if logged:
                query += '?login=true'
            return redirect(query)
