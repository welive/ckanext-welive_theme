import ConfigParser
import json
import logging
import os
import urllib

import requests

import ckan.lib.helpers as h
import ckan.logic as logic
import ckan.model as model
import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import pylons
from ckan.lib.helpers import (_VALID_GRAVATAR_DEFAULTS, gravatar, link_to,
                              url_for)
from pylons import config
from routes.mapper import SubMapper
from webhelpers.html import literal

log = logging.getLogger(__name__)

config_parser = ConfigParser.ConfigParser()
config_parser.read(os.environ['CKAN_CONFIG'])

WELIVE_SECTION = 'plugin:welive_utils'
WELIVE_API = config_parser.get(WELIVE_SECTION, 'welive_api')
PILOT_DICT = json.loads(config_parser.get(WELIVE_SECTION, 'pilot_dict'))

BASIC_USER = config_parser.get(WELIVE_SECTION, 'basic_user')
BASIC_PASSWORD = config_parser.get(WELIVE_SECTION, 'basic_password')

AUTH_SECTION = 'plugin:authentication'
WELIVE_URL = config_parser.get(AUTH_SECTION, 'welive_url')


def get_organizations():
    organization_list = []
    organizations = logic.get_action('organization_list')({}, {})
    for organization_id in organizations:
        if organization_id not in PILOT_DICT.keys():
            context = {'ignore_auth': True,
                       'limits': {'packages': 2},
                       'for_view': True}
            data_dict = {'id': organization_id,
                         'include_datasets': True}
            organization = logic.get_action('organization_show')(context,
                                                                 data_dict)
            organization_list.append(organization)
    return organization_list


def get_org_pilot(organization):
    return PILOT_DICT.get(organization.name, None)


def get_pkg_pilot(pkg):
    organization = logic.get_action('organization_show')({},
                                                         {'id': pkg.owner_org})
    return PILOT_DICT.get(organization.get('name', ''), None)


def get_pilots():
    organization_list = []
    organizations = logic.get_action('organization_list')({}, {})
    for organization_id in organizations:
        if organization_id in PILOT_DICT.keys():
            context = {'ignore_auth': True,
                       'limits': {'packages': 2},
                       'for_view': True}
            data_dict = {'id': organization_id,
                         'include_datasets': True}
            organization = logic.get_action('organization_show')(context,
                                                                 data_dict)
            organization_list.append(organization)
    return organization_list


def get_user_pilot(cc_user_id):
    if cc_user_id is not None and cc_user_id != '':
        response = requests.post(
            '{}/lum/get-user-data/cc-user-id/{}'.format(WELIVE_API,
                                                        int(cc_user_id)),
            auth=(BASIC_USER, BASIC_PASSWORD)).json()

        referred_pilot = response.get('user', {}).get('referredPilot', None)

        return referred_pilot


def get_pilot_from_path(path):
    path_list = path.split('/')
    for item in path_list:
        if item in PILOT_DICT.keys():
            pilot = PILOT_DICT.get(item, None)
            return pilot

    return None


def get_gravatar_dock(email_hash, size=100, default=None):
    if default is None:
        default = config.get('ckan.gravatar_default', 'identicon')

    if default not in _VALID_GRAVATAR_DEFAULTS:
        # treat the default as a url
        default = urllib.quote(default, safe='')

    return literal('''<img src="//gravatar.com/avatar/%s?s=%d&amp;d=%s"
        class="gravatar-dock" width="%s" height="%s" alt="" />'''
                   % (email_hash, size, default, size, size)
                   )


def get_welive_avatar(cc_user_id):
    response = requests.post(
        '{}/lum/get-user-data/cc-user-id/{}'.format(WELIVE_API,
                                                    int(cc_user_id)),
        auth=(BASIC_USER, BASIC_PASSWORD)).json()

    avatar_url = response['user']['avatar']

    return literal('''<img src="%s%s"
        class="gravatar-dock" width="20" height="20" alt="" />'''
                   % (WELIVE_URL, avatar_url)
                   )


def is_developer():
    if toolkit.c.userobj is not None:
        user = toolkit.c.userobj
        response = requests.post("%s/lum/get-user-roles/%s" % (WELIVE_API,
                                                               int(user.name)),
                                 auth=(BASIC_USER, BASIC_PASSWORD))
        if response.status_code == 200:
            try:
                json_response = response.json()
                if not json_response["error"]:
                    if json_response["isDeveloper"]:
                        return True
            except Exception as e:
                log.error(e)
                log.error(response.content)
                return False
    return False


def is_authority():
    if toolkit.c.userobj is not None:
        user = toolkit.c.userobj
        response = requests.post("%s/lum/get-user-roles/%s" % (WELIVE_API,
                                                               int(user.name)),
                                 auth=(BASIC_USER, BASIC_PASSWORD))
        if response.status_code == 200:
            try:
                json_response = response.json()
                if not json_response["error"]:
                    if json_response["role"] == "Authority":
                        return True
            except Exception as e:
                log.error(e)
                log.error(response.content)
    return False


def get_user_display_name(user, complete=False):
    try:
        if (toolkit.c.userobj is not None and
                'ckanext-welive-token' in pylons.session):
            token = pylons.session['ckanext-welive-token']
            if type(user) == dict:
                username = user['name']
            else:
                username = user.name
            response = requests.get(
                '{}/aac/basicprofile/all/{}'.format(WELIVE_API, int(username)),
                headers={'Authorization': token})
            json_response = response.json()
            if not complete:
                return json_response.get('name', None)
            else:
                return '{} {}'.format(json_response.get('name', None),
                                      json_response.get('surname', None))
    except Exception:
        log.debug('Problem when getting info for user {}'.format(user.name))
        return None


def get_display_name(complete=False):
    if (toolkit.c.userobj is not None and
            'ckanext-welive-token' in pylons.session):
        token = pylons.session['ckanext-welive-token']
        response = requests.get(
            '{}/aac/basicprofile/me'.format(WELIVE_API),
            headers={'Authorization': token})
        json_response = response.json()
        if json_response.get('name', None) is not None:
            if not complete:
                return json_response['name']
            else:
                return '{} {}'.format(json_response['name'],
                                      json_response['surname'])
        else:
            response = requests.get(
                '{}/aac/accountprofile/me'.format(WELIVE_API),
                headers={'Authorization': token})
            json_response = response.json()
            if 'username' in json_response.get('accounts', {}).get('welive',
                                                                   {}):
                return json_response['accounts']['welive']['username']


def linked_user(user, maxlength=0, avatar=20):
    if user in [model.PSEUDO_USER__LOGGED_IN, model.PSEUDO_USER__VISITOR]:
        return user
    if not isinstance(user, model.User):
        user_name = unicode(user)
        user = model.User.get(user_name)
        if not user:
            return user_name
    if user:
        name = user.name if model.User.VALID_NAME.match(user.name) else user.id
        icon = gravatar(email_hash=user.email_hash, size=avatar)
        # displayname = user.display_name
        displayname = get_user_display_name(user)
        if displayname is not None:
            if maxlength and len(displayname) > maxlength:
                displayname = displayname[:maxlength] + '...'
        return icon + u' ' + link_to(displayname,
                                     url_for(controller='user', action='read',
                                             id=name))


class Welive_ThemePlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.ITemplateHelpers)
    plugins.implements(plugins.IRoutes, inherit=True)

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'welive_theme')
        toolkit.add_resource('fanstatic', 'terms')

        h.linked_user = linked_user

    # ITemplateHelpers

    def get_helpers(self):
        return {'get_organizations': get_organizations,
                'get_gravatar_dock': get_gravatar_dock,
                'is_developer': is_developer,
                'is_authority': is_authority,
                'get_display_name': get_display_name,
                'get_user_display_name': get_user_display_name,
                'linked_user': linked_user,
                'get_welive_avatar': get_welive_avatar,
                'get_pilots': get_pilots,
                'get_user_pilot': get_user_pilot,
                'get_pilot_from_path': get_pilot_from_path,
                'get_org_pilot': get_org_pilot,
                'get_pkg_pilot': get_pkg_pilot}

    # IRoutes

    def before_map(self, map):
        with SubMapper(
                map,
                controller='ckanext.welive_theme.controller:'
                           'WeLiveThemeController') as m:
            m.connect('cdv', '/welive/redirect/{id}',
                      action='welive_redirect', ckan_icon='cogs')
        with SubMapper(
                map,
                controller='ckanext.welive_theme.controller:'
                'ReportController') as m:
            m.connect('welive_report', '/report-dataset',
                      action='welive_report',
                      ckan_icon='warning-sign')

        return map
